
#ifndef CENCODER_H
#define CENCODER_H

#include <string>
#include <stack>
#include <vector>

#define HAVE_STRUCT_TIMESPEC
#include <pthread.h>

namespace encoder {

	//====================================================================================================================================

	struct  WAV_HEADER {
		char                RIFF[4];        // RIFF Header      Magic header
		unsigned long       ChunkSize;      // RIFF Chunk Size
		char                WAVE[4];        // WAVE Header
		char                fmt[4];         // FMT header
		unsigned long       Subchunk1Size;  // Size of the fmt chunk
		unsigned short      AudioFormat;    // Audio format 1=PCM,6=mulaw,7=alaw, 257=IBM Mu-Law, 258=IBM A-Law, 259=ADPCM
		unsigned short      NumOfChan;      // Number of channels 1=Mono 2=Sterio
		unsigned long       SamplesPerSec;  // Sampling Frequency in Hz
		unsigned long       bytesPerSec;    // bytes per second
		unsigned short      blockAlign;     // 2=16-bit mono, 4=16-bit stereo
		unsigned short      bitsPerSample;  // Number of bits per sample
		char                Subchunk2ID[4]; // "data"  string
		unsigned long       Subchunk2Size;  // Sampled data length
	};

	//====================================================================================================================================

	class RAII_Mutex_Guard
	{
	public:
		RAII_Mutex_Guard() = delete;
		RAII_Mutex_Guard(pthread_mutex_t* pMutex);
		~RAII_Mutex_Guard();

	private:
		pthread_mutex_t* pMutex;
	};

	//====================================================================================================================================
	// One task for encoding
	//====================================================================================================================================

	class CTask
	{
	public:
		CTask() = default;
		CTask(const std::string& sFIn, const std::string& sFOut) { sFileIn = sFIn; sFileOut = sFOut; }

		std::string sFileIn;  // full path to wav file to be encoded
		std::string sFileOut; // full path to new mp3 file to be saved
	};

	//====================================================================================================================================
	// One thread for encoding
	//====================================================================================================================================

	class CMyThread
	{
	public:
		CMyThread() : nTotalTasksSize(0), nTasksNumber(0) { }

		static void * Worker(void * pAttr);

		static void ConvertWavToMP3(const std::string& sFileIn, const std::string& sFileOut, pthread_mutex_t* pScrMutex);

		pthread_t thread;
		static pthread_mutex_t* pScreenMutex; // Static pointer to mutex for accessing to screen from different threads

		uintmax_t nTotalTasksSize; // In bytes
		size_t nTasksNumber; // Number of given tasks, used in join function
		std::stack<CTask> tasks; // Tasks for this thead for encoding
	};

	//====================================================================================================================================
	// Main class for encoding
	//====================================================================================================================================

	class CEncoder {
	public:
		CEncoder();
		~CEncoder();

		void SetCoreNumber(unsigned int nCNum);
		unsigned int LoadTasksFromDir(const std::string & sDir);
		void RunThreads();
		void JoinThreads();
		void PrintEncodingStatistic();

	private:
		unsigned int nCoreNumber;
		pthread_attr_t attr;
		std::vector<CMyThread*> pThreads;

		void AddTask(const CTask & task, uintmax_t nSize);
		void DeleteThreadsVec();
	};

} // namespace

#endif // !CENCODER_H