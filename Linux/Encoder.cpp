
#include "pch.h"
#include <iostream>
#include <experimental/filesystem>
#include <algorithm>
#include <cctype>
#include <cstring>

#define HAVE_STRUCT_TIMESPEC
#include <pthread.h>

#include "BladeMP3Enc.h"
#include "Encoder.h"

using namespace std;
namespace fs = experimental::filesystem;
using namespace encoder;

#ifdef WIN32
const string sSlash = "\\";
#else
const string sSlash = "/";
#endif // WIN32

const char SLASH = 47;
const char BACKSLASH = 92;

const string WAV_EXTENSION = string(".wav");
const string MP3_EXTENSION = string(".mp3");

//====================================================================================================================================
// IMPLEMENTATION OF RAII_Mutex_Guard
//====================================================================================================================================

RAII_Mutex_Guard::RAII_Mutex_Guard(pthread_mutex_t* ptr)
{
	if (ptr != nullptr)
	{
		pMutex = ptr;
		pthread_mutex_lock(pMutex);
	}
	else
	{
		pMutex = nullptr;
	}
}

RAII_Mutex_Guard::~RAII_Mutex_Guard()
{
	if (pMutex != nullptr)
	{
		pthread_mutex_unlock(pMutex);
	}
}

//====================================================================================================================================
// IMPLEMENTATION OF CMyThread
//====================================================================================================================================

pthread_mutex_t* CMyThread::pScreenMutex = nullptr;

void * CMyThread::Worker(void * pAttr)
{
	if (pAttr != nullptr)
	{
		stack<CTask>* pTasks = static_cast<stack<CTask>*>(pAttr);
		CTask CurTask;
		size_t nSolvedTaskNumber = 0;
		while (pTasks->size() > 0)
		{
			CurTask = pTasks->top();
			pTasks->pop();
			ConvertWavToMP3(CurTask.sFileIn, CurTask.sFileOut, pScreenMutex);
			nSolvedTaskNumber++;
		}
		pthread_exit(NULL);
	}
	else
	{
		RAII_Mutex_Guard mutex(CMyThread::pScreenMutex);
		cout << "Error in CMyThread::Worker provide non NULL pointer to thread Tasks" << endl;
	}

	return NULL;
}

//====================================================================================================================================
// Reworked example of using BladeMP3EncDLL interface
// It was written in C style
void CMyThread::ConvertWavToMP3(const string& sFileIn, const string& sFileOut, pthread_mutex_t* pScrMutex)
{
	FILE* pFileIn = NULL;
	FILE* pFileOut = NULL;

	BE_VERSION Version = { 0, };
	BE_CONFIG beConfig = { 0, };

	unsigned long dwSamples = 0;
	unsigned long dwMP3Buffer = 0;
	HBE_STREAM hbeStream = 0;
	BE_ERR err = 0;

	unsigned char * pMP3Buffer = NULL;
	short *pWAVBuffer = NULL;

	// Try to open the WAV file, be sure to open it as a binary file!
	pFileIn = fopen(sFileIn.c_str(), "rb");

	// Check file open result
	if (pFileIn == NULL)
	{
		RAII_Mutex_Guard mutex(pScrMutex);
		fprintf(stderr, "CMyThread::ConvertWavToMP3 Error opening %s", sFileIn.c_str());
		return;
	}

	// Open MP3 file
	pFileOut = fopen(sFileOut.c_str(), "wb+");

	// Check file open result
	if (pFileOut == NULL)
	{
		RAII_Mutex_Guard mutex(pScrMutex);
		fprintf(stderr, "CMyThread::ConvertWavToMP3 Error creating file %s", sFileOut.c_str());
		fclose(pFileIn);
		return;
	}

	WAV_HEADER hr;
	const int hr_size = sizeof(hr);
	memset(&hr, 0, hr_size);             // clear all fields
	fread(&hr, 1, hr_size, pFileIn);

	memset(&beConfig, 0, sizeof(beConfig)); // clear all fields

	// use the LAME config structure
	beConfig.dwConfig = BE_CONFIG_LAME;

	// this are the default settings for testcase.wav
	beConfig.format.LHV1.dwStructVersion = 1;
	beConfig.format.LHV1.dwStructSize = sizeof(beConfig);
	beConfig.format.LHV1.dwSampleRate = hr.SamplesPerSec;	// INPUT FREQUENCY
	beConfig.format.LHV1.dwReSampleRate = 0;					// DON"T RESAMPLE

	if (hr.NumOfChan == 1)
	{
		beConfig.format.LHV1.nMode = BE_MP3_MODE_MONO;
	}
	else if (hr.NumOfChan == 2)
	{
		beConfig.format.LHV1.nMode = BE_MP3_MODE_STEREO;	// OUTPUT IN STREO
	}

	beConfig.format.LHV1.dwBitrate = 128;					// MINIMUM BIT RATE
	beConfig.format.LHV1.nPreset = LQP_HIGH_QUALITY;		// QUALITY PRESET SETTING
	beConfig.format.LHV1.dwMpegVersion = MPEG1;				// MPEG VERSION (I or II)
	beConfig.format.LHV1.dwPsyModel = 0;					// USE DEFAULT PSYCHOACOUSTIC MODEL
	beConfig.format.LHV1.dwEmphasis = 0;					// NO EMPHASIS TURNED ON
	beConfig.format.LHV1.bOriginal = true;					// SET ORIGINAL FLAG
	beConfig.format.LHV1.bWriteVBRHeader = true;					// Write INFO tag

	beConfig.format.LHV1.dwMaxBitrate = 320;					// MAXIMUM BIT RATE
	beConfig.format.LHV1.bCRC = true;					// INSERT CRC
	beConfig.format.LHV1.bCopyright = true;					// SET COPYRIGHT FLAG
	beConfig.format.LHV1.bPrivate = true;					// SET PRIVATE FLAG
	beConfig.format.LHV1.bWriteVBRHeader = true;					// YES, WRITE THE XING VBR HEADER
	beConfig.format.LHV1.bEnableVBR = true;					// USE VBR
	beConfig.format.LHV1.nVBRQuality = 5;					// SET VBR QUALITY
	beConfig.format.LHV1.bNoRes = true;					// No Bit resorvoir

	// Init the MP3 Stream
	err = beInitStream(&beConfig, &dwSamples, &dwMP3Buffer, &hbeStream);

	// Check result
	if (err != BE_ERR_SUCCESSFUL)
	{
		RAII_Mutex_Guard mutex(pScrMutex);
		fprintf(stderr, "CMyThread::ConvertWavToMP3 Error opening encoding stream (%lu)", err);
		fclose(pFileIn);
		fclose(pFileOut);
		return;
	}

	// Allocate MP3 buffer
	pMP3Buffer = new (nothrow) unsigned char[dwMP3Buffer];

	// Allocate WAV buffer
	pWAVBuffer = new (nothrow) short[dwSamples];

	// Check if Buffer are allocated properly
	if (pMP3Buffer == nullptr || pWAVBuffer == nullptr)
	{
		RAII_Mutex_Guard mutex(pScrMutex);
		printf("CMyThread::ConvertWavToMP3 Can not allocate buffers. Out of memory");
		fclose(pFileIn);
		fclose(pFileOut);
		return;
	}

	unsigned long dwRead = 0;
	unsigned long dwWrite = 0;
	unsigned long dwDone = 0;
	unsigned long dwFileSize = 0;

	// Seek to end of file
	fseek(pFileIn, 0, SEEK_END);

	// Get the file size
	dwFileSize = ftell(pFileIn);

	// Seek back to start of WAV file,
	// but skip the first 44 bytes, since that's the WAV header
	fseek(pFileIn, 44, SEEK_SET);


	// Convert All PCM samples
	while ((dwRead = fread(pWAVBuffer, sizeof(short), dwSamples, pFileIn)) > 0)
	{
		// Encode samples
		err = beEncodeChunk(hbeStream, dwRead, pWAVBuffer, pMP3Buffer, &dwWrite);

		// Check result
		if (err != BE_ERR_SUCCESSFUL)
		{
			RAII_Mutex_Guard mutex(pScrMutex);
			beCloseStream(hbeStream);
			fprintf(stderr, "CMyThread::ConvertWavToMP3 beEncodeChunk() failed (%lu)", err);
			return;
		}

		// write dwWrite bytes that are returned in tehe pMP3Buffer to disk
		if (fwrite(pMP3Buffer, 1, dwWrite, pFileOut) != dwWrite)
		{
			RAII_Mutex_Guard mutex(pScrMutex);
			fprintf(stderr, "CMyThread::ConvertWavToMP3 Output file write error");
			return;
		}

		dwDone += dwRead * sizeof(short);
		//printf("Done: %0.2f%%     \r", 100 * (float)dwDone / (float)(dwFileSize));
	}

	// Deinit the stream
	err = beDeinitStream(hbeStream, pMP3Buffer, &dwWrite);

	// Check result
	if (err != BE_ERR_SUCCESSFUL)
	{
		RAII_Mutex_Guard mutex(pScrMutex);
		beCloseStream(hbeStream);
		fprintf(stderr, "CMyThread::ConvertWavToMP3 beExitStream failed (%lu)", err);
		return;
	}

	// Are there any bytes returned from the DeInit call?
	// If so, write them to disk
	if (dwWrite)
	{
		if (fwrite(pMP3Buffer, 1, dwWrite, pFileOut) != dwWrite)
		{
			RAII_Mutex_Guard mutex(pScrMutex);
			fprintf(stderr, "CMyThread::ConvertWavToMP3 Output file write error");
			return;
		}
	}

	// close the MP3 Stream
	beCloseStream(hbeStream);

	// Delete WAV buffer
	delete[] pWAVBuffer;

	// Delete MP3 Buffer
	delete[] pMP3Buffer;

	// Close input file
	fclose(pFileIn);

	// Close output file
	fclose(pFileOut);

	// Write the INFO Tag
	beWriteInfoTag(hbeStream, sFileOut.c_str());
}

//====================================================================================================================================
// IMPLEMENTATION OF CEncoder
//====================================================================================================================================

CEncoder::CEncoder()
	: nCoreNumber(0)
{
	// initialize and set the thread attributes
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);
}

//====================================================================================================================================

void CEncoder::DeleteThreadsVec()
{
	for (auto & t : pThreads)
	{
		if (t != nullptr)
		{
			delete t;
			t = nullptr;
		}
	}
}

//====================================================================================================================================

CEncoder::~CEncoder()
{
	DeleteThreadsVec();
}

//====================================================================================================================================

void CEncoder::SetCoreNumber(unsigned int nCNum)
{
	if (nCNum > 0)
	{
		if (nCoreNumber > 0)
		{
			DeleteThreadsVec();
		}
		nCoreNumber = nCNum;
		pThreads.resize(nCoreNumber);
		for (auto & t : pThreads)
		{
			CMyThread* pThread = new (nothrow) CMyThread;
			if (pThread != nullptr)
			{
				t = pThread;
			}
			else
			{
				cout << "CEncoder::SetCoreNumber Error can not allocate CMyThread" << endl;
			}
		}
	}
}

//====================================================================================================================================

void CEncoder::AddTask(const CTask & task, uintmax_t nTaskSize)
{
	if (nCoreNumber > 0)
	{
		uintmax_t min = 0;
		size_t min_index = 0, i = 0;
		bool bContinue = true;
		for (auto & t : pThreads)
		{
			if (t == nullptr)
			{
				cout << "CEncoder::AddTask Fatal error. Some CMyThread[" << i << "] was not allocated properly." << endl;
				bContinue = false;
				break;
			}

			if (t->nTotalTasksSize == 0) // if some thread is without tasks give current tast to it
			{
				t->tasks.push(task);
				t->nTasksNumber += 1;
				t->nTotalTasksSize += nTaskSize;
				bContinue = false;
				break;
			}
			else
			{
				if (min == 0)
				{
					min = t->nTotalTasksSize;
					min_index = i;
				}
				else
				{
					if (t->nTotalTasksSize < min) // we should find the most free thread
					{
						min = t->nTotalTasksSize;
						min_index = i;
					}
				}
			}

			i += 1;
		}

		if (bContinue) // happens when all threads have tasks, we should use the most free thread (with minimum nTotalTasksSize)
		{
			pThreads[min_index]->tasks.push(task);
			pThreads[min_index]->nTotalTasksSize += nTaskSize;
			pThreads[min_index]->nTasksNumber += 1;
		}
	}
}

//====================================================================================================================================

void CEncoder::RunThreads()
{
	if (nCoreNumber > 0)
	{
		// creating threads 
		for (auto & t : pThreads)
		{
			if ((t != nullptr) && (t->tasks.size() > 0)) // threads with assigned taks will be run only
			{
				int result = pthread_create(&(t->thread), &attr, CMyThread::Worker, (void *)&(t->tasks));
				if (result != 0)
				{
					cout << "CEncoder::RunThreads Creating thread failed!" << endl;
				}
			}
		}
	}
}

//====================================================================================================================================

void CEncoder::JoinThreads()
{
	if (nCoreNumber > 0)
	{
		// joining threads
		for (auto & t : pThreads)
		{
			if ((t != nullptr) && (t->nTasksNumber > 0))
			{
				int result = pthread_join(t->thread, NULL);
				if (result != 0)
				{
					cout << "CEncoder::JoinThreads Joining thread failed!" << endl;
				}
			}
		}
	}
}

//====================================================================================================================================

unsigned int CEncoder::LoadTasksFromDir(const string & sWavRootDirectory)
{
	if (nCoreNumber == 0)
	{
		cout << "Error in CEncoder::LoadTasksFromDir please set number of cores of processor before." << endl;
		return 0;
	}

	// remove last slash if exist
	string sWavRootDir = sWavRootDirectory;
	if (sWavRootDir[sWavRootDir.length() - 1] == BACKSLASH || sWavRootDir[sWavRootDir.length() - 1] == SLASH)
	{
		sWavRootDir.resize(sWavRootDir.length() - 1);
	}
	cout << "Path for encoding: " << sWavRootDir << endl;

	unsigned int nWavFilesNumer = 0;
	if (fs::is_directory(sWavRootDir) && !fs::is_empty(sWavRootDir))
	{
		cout << "The given argumrnt is a dir and it is not empty." << endl;
		cout << "List of wav files:" << endl;
		for (const auto& entry : fs::directory_iterator(sWavRootDir))
		{
			auto filename = entry.path().filename();
			string extension = entry.path().extension().generic_string();
			transform(extension.begin(), extension.end(), extension.begin(), [](unsigned char c){ return std::tolower(c); });
			if (fs::is_regular_file(entry.status()) && extension == WAV_EXTENSION)
			{
				const uintmax_t nWavFileSize = fs::file_size(entry.path());
				cout << filename << ", Size = " << nWavFileSize;
				fs::path outputfilename = filename;
				outputfilename.replace_extension(MP3_EXTENSION);
				cout << ", Output = " << outputfilename << endl;
				string outputpath(sWavRootDir);
				outputpath = outputpath + sSlash + outputfilename.generic_string();
				if (fs::exists(outputpath)) // remove existing mp3 file
				{
					fs::remove(outputpath);
				}

				CTask task(entry.path().generic_string(), outputpath);
				AddTask(task, nWavFileSize);
				nWavFilesNumer++;
			}
		}
	}
	else
	{
		cout << "The given argumrnt is NOT a dir or it IS empty." << endl;
	}

	return nWavFilesNumer;
}

//====================================================================================================================================

void CEncoder::PrintEncodingStatistic()
{
	if (nCoreNumber > 0)
	{
		unsigned int nThreadNumber = 1;
		for (auto & t : pThreads)
		{
			if ((t != nullptr) && (t->nTasksNumber > 0))
			{
				cout << "Thread number " << nThreadNumber << " encoded " << t->nTasksNumber << " wav files, total size is (" << t->nTotalTasksSize << " bytes)." << endl;
				nThreadNumber++;
			}
		}
	}
}