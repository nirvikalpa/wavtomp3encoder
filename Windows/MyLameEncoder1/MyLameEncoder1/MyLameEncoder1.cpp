﻿//====================================================================================================================================
// Note: WAV files are assumed to to have the following parameters
// 44100 Hz, stereo, 16 Bits per sample
//====================================================================================================================================
// To build app for Windows use Visual Studio and MyLameEncoder1.sln
// To build app for Linux (I used Ubuntu) use g++ installed (I used 7.3 version) and build.sh in Linux folder (you will need also some 32 bit libs installed)
//====================================================================================================================================
// Details:
// As I understand, the task is parallel wav to mp3 encoding for group of files. And I implemented this. 
// Because if the task is to encode one big wav file to mp3 in parallel a bit other solution is needed for better efficiency.
//====================================================================================================================================

#include "pch.h"
#include <iostream>
#include <chrono>
#include <thread>
#include <memory>
#define HAVE_STRUCT_TIMESPEC
#include <pthread.h>
#include "Encoder.h"

using namespace std;
using namespace encoder;

const unsigned int CORRECT_COMMAND_LINE_ARG_NUMBER = 2;

//====================================================================================================================================

unsigned int GetCoresNumber()
{
	unsigned int nCoreNumber = thread::hardware_concurrency();
	if (nCoreNumber == 0)
	{
		nCoreNumber = 1; // minimum 1 thread should exist for encoding
	}
	return nCoreNumber;
}

//====================================================================================================================================
// MAIN
//====================================================================================================================================

int main(int argc, char* argv[])
{
	cout << "WavToMP3 encoder..." << endl;
	unsigned int nCoreNumber = GetCoresNumber();

	cout << "NumberOfProcessorCores = " << nCoreNumber << endl << endl;

	unique_ptr<CEncoder> pEncoder(new (nothrow) CEncoder); // Main object for encoding
	if (pEncoder.get() == nullptr)
	{
		return 1;
	}
	pEncoder->SetCoreNumber(nCoreNumber);

	if (argc == CORRECT_COMMAND_LINE_ARG_NUMBER)
	{
		if (pEncoder->LoadTasksFromDir(argv[1]) > 0) // Have we found some wav files in provided dir?
		{
			CMyThread::pScreenMutex = new (nothrow) pthread_mutex_t;
			if (CMyThread::pScreenMutex != nullptr)
			{
				if (pthread_mutex_init(CMyThread::pScreenMutex, NULL) == 0)
				{
					auto time_start = chrono::high_resolution_clock::now();
					pEncoder->RunThreads();
					pEncoder->JoinThreads();
					auto time_end = chrono::high_resolution_clock::now();
					pthread_mutex_destroy(CMyThread::pScreenMutex);
					cout << endl;
					pEncoder->PrintEncodingStatistic();
					double elaspedTimeMs = chrono::duration<double, std::milli>(time_end - time_start).count();
					cout << endl << "Encoding took next time: " << elaspedTimeMs << " ms" << endl;
				}
				if (CMyThread::pScreenMutex != nullptr)
				{
					delete CMyThread::pScreenMutex;
					CMyThread::pScreenMutex = nullptr;
				}
			}
			else
			{
				cout << "Error in main() Can not allocate CMyThread::pScreenMutex Out of memory." << endl;
			}
		}
		else
		{
			cout << "Can not find any wav file in given folder" << endl;
		}
	}
	else
	{
		cout << "Please provide command line argument: wav folder path." << endl;
	}

	cin.get();
	return 0;
}
