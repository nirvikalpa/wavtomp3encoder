WavToMp3Encoder Version 1.0.0.0

Description:
A very simple encoder for music from WAV format to MP3.
It is written in C++. Some features of C++17 were used.
Multithreading supported. All available CPU cores will be used.
For multithreading implementation used Posix Threads (pthreads).
For encoding it is used LAME 3mp static library.
The code can be successfully compiled on Win and on Linux OS.
For Windows to build the code please use the latest version of Microsoft Visual Studio.

<> C++17
<> multithreading
<> pthreads
<> LAME MP3
<> portable (Win, Linux)

(с) Dmitry Sidelnikov